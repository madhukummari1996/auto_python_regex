### Badges
[![regression testing](https://github.com/sede-x/terraform-aws-efs/actions/workflows/regression-testing.yml/badge.svg)](https://github.com/sede-x/terraform-aws-efs/actions/workflows/regression-testing.yml)

# AWS EFS module 
This module requires to be installed:

terraform >=0.14

Documentation framework:
[terraform-docs](https://github.com/terraform-docs/terraform-docs) >= v0.12.1

## Usage example
<!--- BEGIN_USAGE --->
## Usage example
hcl
provider "aws" {
  region = "us-east-1"
}

# Source Code to test 

module "vpc" {

  # CONSUME VPC MODULE FROM INNERSOURCE IAC FOR TESTING
  source = "https://gitlab.com/madhukummari1996/auto_python_regex.git?ref=v1.9"

  environment              = "regression-testing"
  vpc_name                 = "regression-testing-vpc"
  cidr_block               = "10.0.0.0/16"
  create_public_subnets    = true
  public_subnets_name      = "regression-testing-public_subnets"
  availability_zone_public = ["us-east-1a", "us-east-1b"]
  public_subnets           = ["10.0.0.128/26", "10.0.0.192/26"]
  tags = {
    Regression_Testing = "terraform-aws-efs_DELETE"
    Schedule_Delete    = "yes"
  }
}

module "efs" {
 	source 									=	"https://gitlab.com/madhukummari1996/auto_python_regex.git?ref=v1.0"
  creation_token          = "regression-testing-efs"
  environment             = "regression-testing"
  subnet_ids              = [module.vpc.public_subnets_id[0], module.vpc.public_subnets_id[1]]
  security_groups         = [module.vpc.default_security_group_id[0]]
  create_efs_mount_target = true
  tags = {
    Regression_Testing = "terraform-aws-efs_DELETE"
    Schedule_Delete    = "yes"
  }
}


<!--- END_USAGE --->

<!--- BEGIN_TF_DOCS --->
<details><summary>Requirements</summary>


| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 3.36.0 |
</details>
<details><summary>Providers</summary>


No providers.
</details>
<details><summary>Modules</summary>


| Name | Source | Version |
|------|--------|---------|
| <a name="module_efs"></a> [efs](#module\_efs) | ./modules/efs | n/a |
</details>
<details><summary>Inputs</summary>


| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_create_efs_mount_target"></a> [create\_efs\_mount\_target](#input\_create\_efs\_mount\_target) | Make it true to create efs\_mount\_target | `bool` | `false` | no |
| <a name="input_creation_token"></a> [creation\_token](#input\_creation\_token) | A unique name (a maximum of 64 characters are allowed) used as reference when creating the Elastic File System to ensure idempotent file system creation. | `string` | n/a | yes |
| <a name="input_efs_name"></a> [efs\_name](#input\_efs\_name) | Name for EFS | `string` | `"default-efs"` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment name | `string` | `"default"` | no |
| <a name="input_security_groups"></a> [security\_groups](#input\_security\_groups) | Security group IDs to allow access to the EFS | `list(string)` | `[]` | no |
| <a name="input_subnet_ids"></a> [subnet\_ids](#input\_subnet\_ids) | Subnet IDs | `list(string)` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Map of user-defined tags | `map(string)` | `{}` | no |
| <a name="input_transition_to_ia"></a> [transition\_to\_ia](#input\_transition\_to\_ia) | Indicates how long it takes to transition files to the IA storage class. | `string` | `""` | no |
</details>
<details><summary>Outputs</summary>


| Name | Description |
|------|-------------|
| <a name="output_arn"></a> [arn](#output\_arn) | ARN of EFS |
| <a name="output_file_system_dns_name"></a> [file\_system\_dns\_name](#output\_file\_system\_dns\_name) | DNS name of EFS |
| <a name="output_file_system_id"></a> [file…